package hackathon.mappingcircle.utils;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class FileUtil {

	 public static String getFileAsStringByPath(String filePath) {
		    String documentAsString = null;
		    if (Paths.get(filePath).toFile().exists())
		      try (BufferedReader reader = Files.newBufferedReader(Paths.get(filePath))) {
		        StringBuilder out = new StringBuilder();
		        String line;
		        while ((line = reader.readLine()) != null)
		          out.append(line);
		        documentAsString = out.toString();
		        reader.close();
		      } catch (FileNotFoundException e) {
		    	  e.printStackTrace();
		      } catch (IOException e) {
		    	  e.printStackTrace();
		      }
		    return documentAsString;
		  }
	 
	 public static boolean saveFile(String testData, String processValue) {
		    if (!testData.isEmpty()) {
		      String directoryPath = testData.substring(0, testData.lastIndexOf('/'));
		      String tempFileName = testData.substring(testData.lastIndexOf('/') + 1);
		      try {
		        Path pathTxtFileDir = Paths.get(directoryPath);
		        pathTxtFileDir = Paths.get(pathTxtFileDir.toFile().getCanonicalPath());

		        if (!pathTxtFileDir.toFile().exists())
		          Files.createDirectories(pathTxtFileDir);

		        Path pathTxtFile = Paths.get(pathTxtFileDir.toAbsolutePath().toString(), tempFileName);
		        Files.write(pathTxtFile, processValue.getBytes());
		        return true;
		      } catch (IOException e1) {
		    	  e1.printStackTrace();
		        return false;
		      }
		    } else
		      return false;
		  }
	 
	 public static List<String> realAlllines(String filepath){
		 Path wiki_path = Paths.get(filepath);
		 List<String> lines=new ArrayList<>() ;
		    Charset charset = Charset.forName("ISO-8859-1");
		    try {
		      lines = Files.readAllLines(wiki_path, charset);
		    }catch(Exception e) {
		    	e.printStackTrace();
		    }
		    return lines;
	 }
	 
	 public static void writeLineBybLine(List<String> allLines,String filepath){
		 try {
			    File fout = new File(filepath);
				FileOutputStream fos = new FileOutputStream(fout);
			 
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
			 
				for (String line:allLines) {
					bw.write(line);
					bw.newLine();
				}
			 
				bw.close();
		 }catch (Exception e) {
			 e.printStackTrace();
		}
		
	 }
	
	public static void main(String[] args) {
		String outputpath="D:/learning/out.xml";
		String path = System.getProperty("user.dir") + File.separator + "src/test/resources/XmlFiles/GamePlayTemplate.xml";
		String output = System.getProperty("user.dir") + File.separator + "src/test/resources/XmlFiles/GamePlayTemplateNew.xml";
		System.out.println(path);
		String inputfile=getFileAsStringByPath(path);
		
		String testcase="\"verfiyEndToendForCashGameT20\"";
		String existing="exclude name="+testcase;
		String newValue="include name="+testcase;
		inputfile=inputfile.replaceAll(existing, newValue);
		System.out.println(inputfile);
		saveFile(output,inputfile);
		
	}

}
