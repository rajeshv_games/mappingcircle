package hackathon.mappingcircle.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;



public class DataBaseUtil {
	
	static Connection connection = null;
	static Properties configProp = PropertyReader.loadProertyFile( "mappingcircle_config.properties" );
	
	public static boolean openConnection() throws ClassNotFoundException, Exception
	{

		if( isConnectionClosed() )
		{
			String url = configProp.getProperty( "reverie.jdbc.url" );
			String username = configProp.getProperty( "reverie.jdbc.username" );
			String password = configProp.getProperty( "reverie.jdbc.password" );
			try
			{
				Class.forName( configProp.getProperty( "reverie.jdbc.drivers" ) );
				connection = DriverManager.getConnection( url, username, password );
			}
			catch( SQLException e )
			{
				e.printStackTrace();
			}
		}
		return connection != null;
	}

	public static boolean isConnectionClosed()
	{
		try
		{
			return connection == null || connection.isClosed();
		}
		catch( SQLException e )
		{
			e.printStackTrace();
			return true;
		}
	}

	public static void closeConnection()
	{
		if( connection != null )
		{
			try
			{
				connection.close();
			}
			catch( SQLException e )
			{
				e.printStackTrace();
			}
		}
	}

	/**
	 * Execute query
	 * 
	 * @param query
	 * @return
	 */
	public static Object[][] executeQuery( String query )
	{
		if( connection == null )
		{
			System.err.println( "Connection is null" );
			return null;
		}
		ResultSet result = null;
		ResultSetMetaData rsMetaData = null;
		Statement stat;
		Object[][] finalResult = null;
		try
		{
			stat = connection.createStatement();
			System.out.println( "My query: " + query );
			result = stat.executeQuery( query );
			rsMetaData = result.getMetaData();
			int columnCount = rsMetaData.getColumnCount();
			ArrayList< Object[] > data = new ArrayList< Object[] >();
			Object[] header = new Object[columnCount];
			for( int i = 1; i <= columnCount; ++i )
			{
				Object label = rsMetaData.getColumnLabel( i );
				header[i - 1] = label;
			}
			while( result.next() )
			{
				Object[] str = new Object[columnCount];
				for( int i = 1; i <= columnCount; ++i )
				{
					Object obj;
					obj = result.getObject( i );
					str[i - 1] = obj;

				}
				data.add( str );
			}
			int resultLength = data.size();
			finalResult = new Object[resultLength][columnCount];
			for( int i = 0; i < resultLength; ++i )
			{
				Object[] row = data.get( i );
				finalResult[i] = row;
			}
		}
		catch( SQLException e )
		{
			e.printStackTrace();
		}
		return finalResult;
	}

	/**
	 * Update query
	 * 
	 * @param query
	 * @return
	 */
	
	public static void executeUpdate( String query )
	{
		Statement stat;
		try
		{
			if( connection == null )
			{
				System.err.println( "Connection is null" );
				return;
			}
			stat = connection.createStatement();
			stat.executeUpdate( query );
		}
		catch( SQLException e )
		{
			e.printStackTrace();
		}
	}
	
	public static void delete(String query) {
		Statement stat;
		try
		{
			if( connection == null )
			{
				System.err.println( "Connection is null" );
				return;
			}
			stat = connection.createStatement();
			stat.execute(query);
		}
		catch( SQLException e )
		{
			e.printStackTrace();
		}
	}

	public static void commit() {
		try {
			connection.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
