package hackathon.mappingcircle.utils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import hackathon.mappingcircle.CoverageData;

public class ReadCSVData {
	
	
	public static void insertFmsData(String testcase,String packagename,String classpath,int lineCove) {
		String query = "INSERT INTO fms_testcase_mapping VALUES('"+testcase+"',"+"'"+packagename+"','"+classpath+"',"+lineCove+")";
  
		try {
			System.out.println("quesry is "+query);
			DataBaseUtil.executeUpdate(query);
			System.out.println("done");
			//DataBaseUtil.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
    
	public static void updateCSVdataToDB(String path,String testcasename) {
		List<CoverageData> coverageDataList=getAllCoveregeData(path);
		try {
			DataBaseUtil.openConnection();
			for(CoverageData coverdata:coverageDataList) {
				insertFmsData(testcasename,coverdata.getPackageName(),coverdata.getFullClassPath(),Integer.parseInt(coverdata.getLineCovered()));
			}
			DataBaseUtil.closeConnection();
		}catch (Exception e) {
		}
		
	}
		
	
	public static List<CoverageData> getAllCoveregeData(String filepath){
		
		List<CoverageData> coverageDataList=new ArrayList<>();
		Path wiki_path = Paths.get(filepath);
	    Charset charset = Charset.forName("ISO-8859-1");
	    String testCaseName="test";
	    try {
	      List<String> lines = Files.readAllLines(wiki_path, charset);
	      final String DELIMITER = ",";
	      
	      for ( int i=1;i<lines.size();i++) {
	    	  String line=lines.get(i);
	        System.out.println(line);
	        String[] tokens = line.split(DELIMITER);
            if(Integer.parseInt(tokens[8]) == 0) {
            	System.out.println("Zero");
                continue;
            }
            else {
            CoverageData cData = new CoverageData(testCaseName,tokens[1],tokens[2],tokens[8]);
            coverageDataList.add(cData);
            }
	      }
	      System.out.println(coverageDataList.size());
	    } catch (IOException e) {
	      System.out.println(e);
	    }
	    return coverageDataList;
	}
public static void main(String[] args) {
	//getAllCoveregeData("");
	String filepath = System.getProperty("user.dir") + File.separator + "src/test/resources/config/"+"coverage-report.csv";
	updateCSVdataToDB(args[1],args[0]);
}
}
