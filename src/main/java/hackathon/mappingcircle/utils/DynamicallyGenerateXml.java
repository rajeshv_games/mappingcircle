package hackathon.mappingcircle.utils;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DynamicallyGenerateXml {
	public static ArrayList<String>classwithouttestcase=new ArrayList<>();

	public static Set<String> getAllTestCase(List<String> appclasspaths) {
		try {
			Set<String> set = new HashSet<>();
			DataBaseUtil.openConnection();
			for (String appclasspath : appclasspaths) {
				String query = "select * from fms_testcase_mapping where classpath='" + appclasspath + "'";
				Object[][] testcase = DataBaseUtil.executeQuery(query);
				System.out.println(testcase.length);
				if (null != testcase && testcase.length > 0) {
					for (int i = 0; i < testcase.length; i++) {
						set.add(testcase[i][0].toString());
						System.out.println(testcase[i][0].toString());
					}
				}else {
					classwithouttestcase.add(appclasspath);
				}
			}
			System.out.println("set size is " + set.size());
			return set;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void generateXml(String xmlpath, Set<String> testcses) {
		try {
			String path = System.getProperty("user.dir") + File.separator
					+ "src/test/resources/XmlFiles/GamePlayTemplate.xml";
			// xmlpath = System.getProperty("user.dir") + File.separator +
			// "src/test/resources/XmlFiles/GamePlayTemplateNew.xml";
			System.out.println(path);
			String inputfile = FileUtil.getFileAsStringByPath(path);
			for (String testcase : testcses) {
				testcase = "\"" + testcase + "\"";
				String existing = "exclude name=" + testcase;
				String newValue = "include name=" + testcase;
				inputfile = inputfile.replaceAll(existing, newValue);
			}
			System.out.println(inputfile);
			FileUtil.saveFile(xmlpath, inputfile);
		} catch (Exception e) {

		}

	}

	public static String replaceForwardSlash(String str) {
		str = str.substring(str.indexOf("com"), str.length()).replaceAll("/", ".");
		str=str.replace(".java", "");
		return str;
	}

	public static void insertFmsBuildDetails(String buildinfo, String packagename, String testcases) {
		String query = "INSERT INTO fms_build_details (build_details,app_classes,testcase_list) VALUES('" + buildinfo
				+ "','" + packagename + "','" + testcases + "')";
		try {
			System.out.println("quesry is " + query);
			DataBaseUtil.executeUpdate(query);
			System.out.println("done");
			// DataBaseUtil.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void getHtmlReport(String appclasses, String buildinfo) {
		System.out.println("appclases    ...." + appclasses);
		System.out.println("build    ...." + buildinfo);
		String[] appclasspaths = appclasses.split("\\s*,\\s*");
		List<String> appclasspathlist = new ArrayList<>();
		for (String appclasspath : appclasspaths) {
			appclasspathlist.add(replaceForwardSlash(appclasspath));
		}
		Set<String> testcases = getAllTestCase(appclasspathlist);
		List<String> testcaselist = Arrays.asList(testcases.toArray(new String[0]));
		System.out.println("tetscase lis is >>>" + testcaselist.toString());
		insertFmsBuildDetails(buildinfo, appclasspathlist.toString(), testcaselist.toString());
		String path = System.getProperty("user.dir") + File.separator + "src/test/resources/config/demo.html";
		String outpath = System.getProperty("user.dir") + File.separator + "index.html";
		System.out.println(path);
		List<String> inputfile = FileUtil.realAlllines(path);
		String[] allLines = inputfile.toArray(new String[inputfile.size()]);
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		String currendate = "Execution Time: " + format.format(date);
		Map<String, String> map = new HashMap<>();
		String build="BUILD: "+buildinfo;
		String testcaseshtml="";
		String classpathshtml="";
		for(String testcasehtml:testcases) {
			testcaseshtml=testcaseshtml+testcasehtml+" </br>";
		}
		for(String classpathhtml:appclasspathlist) {
			classpathshtml=classpathshtml+classpathhtml+" </br>";
		}
		map.put("$buildinfo", build);
		map.put("$execution", currendate);
		map.put("$appclass", classpathshtml);
		map.put("$testcase", testcaseshtml);
		map.put("$classwithouttestcase", classwithouttestcase.toString());
		for (Map.Entry<String, String> entry : map.entrySet()) {
			for (int i = 0; i < inputfile.size(); i++) {
				String line = inputfile.get(i);
				if (line.contains(entry.getKey())) {
					line = line.replace(entry.getKey(), entry.getValue());
					allLines[i] = line;
				}
			}
		}

		List<String> finallist = new ArrayList<>(Arrays.asList(allLines));

		FileUtil.writeLineBybLine(finallist, outpath);
	}

	public static void main(String[] args) {
		try {

			getHtmlReport(args[0], args[1]);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
