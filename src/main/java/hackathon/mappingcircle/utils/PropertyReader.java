package hackathon.mappingcircle.utils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;


public class PropertyReader {

	public static Properties loadProertyFile(String fileName) {
		
		String filepath = System.getProperty("user.dir") + File.separator + "src/test/resources/config/"+fileName;
		Properties prop = new Properties();
		InputStream istr = null;
		try {
			istr = new FileInputStream(filepath);

		} catch (FileNotFoundException e1) {
			System.out.println(e1.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		if (istr != null) {

			try {
				prop.load(istr);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return prop;

	}

}
