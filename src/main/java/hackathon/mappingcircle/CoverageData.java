package hackathon.mappingcircle;

public class CoverageData{
	
	private String testCaseName="";
	private String group = "";
	private String packageName = "";
    private String className = ""; 
    private String instructionMissed = ""; 
    private String instructionCovered = ""; 
    private String branchMissed = ""; 
    private String branchCovered = ""; 
    private String lineMissed = ""; 
    private String lineCovered = ""; 
    private String complexityMissed = ""; 
    private String complexityCovered = ""; 
    private String methodMissed = ""; 
    private String methodCovered = "";
    private String fullClassPath = ""; 
    
    public CoverageData (String testCaseName,String packageName, String className, String lineCovered) {
        	this.packageName = packageName;
        	this.className = className;
        	this.lineCovered = lineCovered;
        	this.fullClassPath = packageName + "." + className;
        	this.testCaseName=testCaseName;
    }
    

	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getInstructionMissed() {
		return instructionMissed;
	}
	public void setInstructionMissed(String instructionMissed) {
		this.instructionMissed = instructionMissed;
	}
	public String getInstructionCovered() {
		return instructionCovered;
	}
	public void setInstructionCovered(String instructionCovered) {
		this.instructionCovered = instructionCovered;
	}
	public String getBranchMissed() {
		return branchMissed;
	}
	public void setBranchMissed(String branchMissed) {
		this.branchMissed = branchMissed;
	}
	public String getBranchCovered() {
		return branchCovered;
	}
	public void setBranchCovered(String branchCovered) {
		this.branchCovered = branchCovered;
	}
	public String getLineMissed() {
		return lineMissed;
	}
	public void setLineMissed(String lineMissed) {
		this.lineMissed = lineMissed;
	}
	public String getLineCovered() {
		return lineCovered;
	}
	public void setLineCovered(String lineCovered) {
		this.lineCovered = lineCovered;
	}
	public String getComplexityMissed() {
		return complexityMissed;
	}
	public void setComplexityMissed(String complexityMissed) {
		this.complexityMissed = complexityMissed;
	}
	public String getComplexityCovered() {
		return complexityCovered;
	}
	public void setComplexityCovered(String complexityCovered) {
		this.complexityCovered = complexityCovered;
	}
	public String getMethodMissed() {
		return methodMissed;
	}
	public void setMethodMissed(String methodMissed) {
		this.methodMissed = methodMissed;
	}
	public String getMethodCovered() {
		return methodCovered;
	}
	public void setMethodCovered(String methodCovered) {
		this.methodCovered = methodCovered;
	}
	public String getFullClassPath() {
		return fullClassPath;
	}
	public String getTestCaseName() {
		return testCaseName;
	}

	public void setTestCaseName(String testCaseName) {
		this.testCaseName = testCaseName;
	}
    
}